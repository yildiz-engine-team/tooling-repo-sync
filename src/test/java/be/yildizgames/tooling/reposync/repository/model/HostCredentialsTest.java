package be.yildizgames.tooling.reposync.repository.model;

import be.yildizgames.common.exception.implementation.ImplementationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class HostCredentialsTest {

    @Nested
    class Constructor {

        @Test
        void happyFlow() {
            HostCredentials credentials = new HostCredentials("value1", "value2", "ciToken");
            Assertions.assertEquals("value1", credentials.login);
            Assertions.assertEquals("value2", credentials.password);
            Assertions.assertEquals("ciToken", credentials.ciApiToken);
        }

        @Test
        void withNullLogin() {
            Assertions.assertThrows(ImplementationException.class, () -> new HostCredentials(null, "value2", "ciToken"));
        }

        @Test
        void withNullPassword() {
            Assertions.assertThrows(ImplementationException.class, () -> new HostCredentials("value1", null, "ciToken"));
        }

    }
}
