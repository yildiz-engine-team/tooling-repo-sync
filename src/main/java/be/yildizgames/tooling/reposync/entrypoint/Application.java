/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */
package be.yildizgames.tooling.reposync.entrypoint;

import be.yildizgames.tooling.reposync.config.RepoSyncConfiguration;
import be.yildizgames.tooling.reposync.repository.model.DistantRepository;
import be.yildizgames.tooling.reposync.repository.model.LocalRepository;
import be.yildizgames.tooling.reposync.repository.model.Repository;
import be.yildizgames.tooling.reposync.repository.model.RepositoryHost;
import be.yildizgames.tooling.reposync.repository.model.RepositoryHosts;

import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Grégory Van den Borre
 */
class Application {

    private final RepoSyncConfiguration configuration;

    private Application(RepoSyncConfiguration configuration) {
        super();
        this.configuration = configuration;
        RepositoryHosts.BITBUCKET.setCredential(configuration.getBitbucket());
        RepositoryHosts.GITHUB.setCredential(configuration.getGithub());
    }

    static Application fromArgs(String[] args) {
        RepoSyncConfiguration config = RepoSyncConfiguration.fromFile(args != null && args.length > 0 ?
                Paths.get(args[0]) : Paths.get("config.properties"));
        return new Application(config);
    }

    void syncRepos(RepositoryHost from, RepositoryHost to) {
        from
                .getRepositoryList()
                .stream()
                .filter(r -> ! this.configuration.isIgnored(r))
                .forEach(r ->
                        r.cloneOrUpdate(from, this.configuration.getRootProjectsPath())
                                .addRemote(to)
                                .push(to));
    }

    void syncRepo(RepositoryHost from, RepositoryHost to, Repository repo) {
        repo.cloneOrUpdate(from, this.configuration.getRootProjectsPath())
                .addRemote(to)
                .push(to);
    }

    List<DistantRepository> listRepoOn(RepositoryHost host) {
        return host.getRepositoryList();
    }

    LocalRepository cloneOrUpdateRepository(Repository r, RepositoryHost host) {
        return r.cloneOrUpdate(host, this.configuration.getRootProjectsPath());
    }

    String getRootProjectsPath() {
        return this.configuration.getRootProjectsPath();
    }

    List<LocalRepository> cloneOrUpdateAllRepositories(RepositoryHost host) {
        return this.listRepoOn(host)
                .stream()
                .filter(r -> ! this.configuration.isIgnored(r))
                .map(r -> cloneOrUpdateRepository(r, host))
                .collect(Collectors.toList());

    }
}
