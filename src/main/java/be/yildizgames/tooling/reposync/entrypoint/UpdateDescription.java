package be.yildizgames.tooling.reposync.entrypoint;

import be.yildizgames.tooling.reposync.repository.model.DistantRepository;
import be.yildizgames.tooling.reposync.repository.model.RepositoryHosts;

public class UpdateDescription {

    public static void main(String[] args) {

        DistantRepository r = Application.fromArgs(args)
                .listRepoOn(RepositoryHosts.GITHUB)
                .stream()
                .filter(dr -> dr.getName().equals("common-application"))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
        RepositoryHosts.GITHUB.setDescription("test", r);
    }
}
