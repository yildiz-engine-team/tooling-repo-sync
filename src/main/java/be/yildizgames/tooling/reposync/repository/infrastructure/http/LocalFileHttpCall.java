/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */
package be.yildizgames.tooling.reposync.repository.infrastructure.http;

import be.yildizgames.common.logging.LogFactory;
import org.slf4j.Logger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;

/**
 * @author Grégory Van den Borre
 */
public class LocalFileHttpCall implements HttpCall {

    private static final Logger LOGGER = LogFactory.getInstance().getLogger(LocalFileHttpCall.class);

    @Override
    public HttpCall authenticateWith(String user, char[] password) {
        return this;
    }

    @Override
    public HttpResponse get(String address) {
        try {
            return new HttpResponse(Collections.emptyMap(), 200, Files.readAllLines(Paths.get("/home/gregory/data"), StandardCharsets.UTF_8).get(0));
        } catch (IOException e) {
            LOGGER.error("Error Local file HTTP GET ", e);
            return new HttpResponse(Collections.emptyMap(), 500,"Error");
        }
    }

    @Override
    public void put(String address, String data) {
        // Intended to be empty.
    }

    @Override
    public void patch(String address, String data) {
        // Intended to be empty.
    }
}
