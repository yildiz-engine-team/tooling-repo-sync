/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */
package be.yildizgames.tooling.reposync.repository.model;

import be.yildizgames.tooling.reposync.repository.infrastructure.http.HttpCall;
import be.yildizgames.tooling.reposync.repository.infrastructure.http.HttpCallerFactory;
import be.yildizgames.tooling.reposync.repository.infrastructure.parser.Parser;
import be.yildizgames.tooling.reposync.repository.infrastructure.parser.ParserFactory;

/**
 * @author Grégory Van den Borre
 */
public abstract class BaseHost implements RepositoryHost {

    private String user;

    private char[] password;

    private String ciApiToken;

    protected final HttpCall caller = HttpCallerFactory.getCaller();

    protected final Parser parser = ParserFactory.getParser();

    @Override
    public final void setCredential(HostCredentials credentials) {
        this.user = credentials.login;
        this.password = credentials.password.toCharArray();
        this.ciApiToken = credentials.ciApiToken;
    }

    @Override
    public final String getCiApiToken() {
        return this.ciApiToken;
    }

    @Override
    public final String getLogin() {
        return this.user;
    }

    @Override
    public final char[] getPassword() {
        return password;
    }
}
