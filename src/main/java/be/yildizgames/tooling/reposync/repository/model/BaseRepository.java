/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */
package be.yildizgames.tooling.reposync.repository.model;

import be.yildizgames.common.exception.implementation.ImplementationException;
import be.yildizgames.common.logging.LogFactory;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.slf4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

/**
 * A repository is the source code hosted on a SCM server.
 * Once cloned, it will create a LocalRepository.
 *
 * @author Grégory Van den Borre
 */
public class BaseRepository implements Repository {

    private static final Logger LOGGER = LogFactory.getInstance().getLogger(BaseRepository.class);

    /**
     * Repository name.
     */
    private final String name;

    private BaseRepository(String name) {
        super();
        ImplementationException.throwForNull(name);
        this.name = name;
    }

    public static Repository fromName(String name) {
        return new BaseRepository(name);
    }

    @Override
    public String getName() {
        return this.name;
    }

    /**
     * Clone the repository locally from a host to a specific folder.
     * @param host Host containing the repository.
     * @param localRoot Directory where the repository will be cloned.
     * @return The created LocalRepository.
     */
    public LocalRepository clone(final RepositoryHost host, final String localRoot) {
        ImplementationException.throwForNull(host);
        ImplementationException.throwForNull(localRoot);

        LocalRepository localRepository = new LocalRepository(this, localRoot);
        Path path = localRepository.getPath();
        try {
            Files.createDirectories(path);
            Git.cloneRepository()
                    .setURI(host.getUrl(this.name))
                    .setDirectory(path.toFile())
                    .call();
        } catch (IOException | GitAPIException e) {
            LOGGER.error("Cloning ", e);
        }
        return localRepository;
    }

    /**
     * Clone the repository locally from a host to a specific folder, or update if the directory already exists.
     * @param host Host containing the repository.
     * @param localRoot Directory where the repository will be cloned or updated.
     * @return The created LocalRepository.
     */
    @Override
    public LocalRepository cloneOrUpdate(RepositoryHost host, String localRoot) {
        ImplementationException.throwForNull(host);
        ImplementationException.throwForNull(localRoot);

        LocalRepository localRepository = new LocalRepository(this, localRoot);
        Path path = localRepository.getPath();
        if(Files.exists(path)) {
            LOGGER.info("Updating {}", this.getName());
            return localRepository.pull(host);
        }
        LOGGER.info("Cloning {}", this.getName());
        return this.clone(host, localRoot);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BaseRepository that = (BaseRepository) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return this.name;
    }
}
