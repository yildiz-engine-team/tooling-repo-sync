/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */
package be.yildizgames.tooling.reposync.repository.model;

import be.yildizgames.tooling.reposync.repository.model.bitbucket.Bitbucket;
import be.yildizgames.tooling.reposync.repository.model.github.Github;

import java.util.List;

/**
 * @author Grégory Van den Borre
 */
public class RepositoryHosts implements RepositoryHost{

    public static final RepositoryHost BITBUCKET = new Bitbucket();

    public static final RepositoryHost GITHUB = new Github();

    public static final RepositoryHost GITLAB = new Gitlab();

    private final RepositoryHost host;

    RepositoryHosts(RepositoryHost host) {
        this.host = host;
    }

    @Override
    public void setCredential(HostCredentials credentials) {
        this.host.setCredential(credentials);
    }

    @Override
    public String getDomain() {
        return this.host.getDomain();
    }

    @Override
    public String getUser() {
        return this.host.getUser();
    }

    @Override
    public String getUrl(String repoName) {
        return this.host.getUrl(repoName);
    }

    @Override
    public String getTeam() {
        return this.host.getTeam();
    }

    @Override
    public List<DistantRepository> getRepositoryList() {
        return this.host.getRepositoryList();
    }

    @Override
    public void setDescription(String description, DistantRepository r) {
        this.host.setDescription(description, r);
    }

    @Override
    public void addEnvVariable(String key, String value, DistantRepository r) {
        this.host.addEnvVariable(key, value, r);
    }

    @Override
    public String getLogin() {
        return this.host.getLogin();
    }

    @Override
    public char[] getPassword() {
        return this.host.getPassword();
    }

    @Override
    public String getCiApiToken() {
        return this.host.getCiApiToken();
    }

    @Override
    public String getName() {
        return this.host.getName();
    }
}
