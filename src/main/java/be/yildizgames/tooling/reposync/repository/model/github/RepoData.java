package be.yildizgames.tooling.reposync.repository.model.github;

import com.google.gson.annotations.Expose;

class RepoData {

    @Expose
    private String id;

    @Expose
    private String name;

    public String getName() {
        return this.name;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }
}
