/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */
package be.yildizgames.tooling.reposync.repository.model;

import be.yildizgames.common.exception.implementation.ImplementationException;
import be.yildizgames.common.logging.LogFactory;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.RemoteAddCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.transport.URIish;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.slf4j.Logger;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/**
 * @author Grégory Van den Borre
 */
public class LocalRepository implements Repository {

    private static final Logger LOGGER = LogFactory.getInstance().getLogger(LogFactory.class);

    private final Repository repository;

    private final String localRoot;

    LocalRepository(Repository repository, String localRoot) {
        super();
        ImplementationException.throwForNull(repository);
        ImplementationException.throwForNull(localRoot);
        this.repository = repository;
        this.localRoot = localRoot;
    }

    Path getPath() {
        return Paths.get(localRoot, this.repository.getName());
    }

    public LocalRepository push(RepositoryHost host) {
        ImplementationException.throwForNull(host);
        try (Git git = Git.open(this.getPath().toFile())) {
            git
                    .push()
                    .setCredentialsProvider(new UsernamePasswordCredentialsProvider(host.getLogin(), host.getPassword()))
                    .setRemote(host.getName())
                    .call();
        } catch (IOException | GitAPIException e) {
            LOGGER.error("Error while pushing.", e);
        }
        return this;
    }

    public LocalRepository pull(RepositoryHost host) {
        ImplementationException.throwForNull(host);
        try (Git git = Git.open(this.getPath().toFile())) {
            git
                    .pull()
                    .setRemote(host.getName())
                    .call();
        } catch (IOException | GitAPIException e) {
            LOGGER.error("Error while pulling.", e);
        }
        return this;
    }

    public LocalRepository addRemote(RepositoryHost host) {
        ImplementationException.throwForNull(host);
        try (Git git = Git.open(this.getPath().toFile())) {
            RemoteAddCommand remoteAddCommand = git.remoteAdd();
            remoteAddCommand.setName(host.getName());
            remoteAddCommand.setUri(new URIish(host.getUrl(this.repository.getName())));
            remoteAddCommand.call();
        } catch (GitAPIException | IOException | URISyntaxException e) {
            LOGGER.error("Error while adding a remote.", e);
        }
        return this;
    }

    public LocalRepository replaceFile(Path original) {
        ImplementationException.throwForNull(original);
        try {
            String fileName = original.toFile().getName();
            Paths.get(this.localRoot, fileName);
            Files.copy(original, Paths.get(this.localRoot, fileName), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            LOGGER.error("Error while replacing a file.", e);
        }
        return this;

    }

    @Override
    public String getName() {
        return this.repository.getName();
    }

    @Override
    public LocalRepository cloneOrUpdate(RepositoryHost from, String rootProjectsPath) {
        return this.repository.cloneOrUpdate(from, rootProjectsPath);
    }
}
