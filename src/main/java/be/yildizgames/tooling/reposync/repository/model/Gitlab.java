/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */
package be.yildizgames.tooling.reposync.repository.model;

import java.util.Collections;
import java.util.List;

/**
 * @author Grégory Van den Borre
 */
public class Gitlab extends BaseHost {

    @Override
    public String getDomain() {
        return "";
    }

    @Override
    public String getUser() {
        return "";
    }

    @Override
    public String getUrl(String repoName) {
        return "";
    }

    @Override
    public String getTeam() {
        return "";
    }

    @Override
    public List<DistantRepository> getRepositoryList() {
        return Collections.emptyList();
    }

    @Override
    public void setDescription(String description, DistantRepository r) {
        //TODO implements.
    }

    @Override
    public void addEnvVariable(String key, String value, DistantRepository r) {
        //TODO implements.
    }

    @Override
    public String getName() {
        return "gitlab";
    }
}
