package be.yildizgames.tooling.reposync.repository.infrastructure.http;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class HttpResponse {

    private final Map<String, List<String>> headers;

    private final int code;

    private final String content;

    HttpResponse(Map<String, List<String>> headers, int code, String content) {
        this.headers = headers;
        this.code = code;
        this.content = content;
    }

    public String getContent() {
        return this.content;
    }

    public Map<String, List<String>> getHeaders() {
        return Collections.unmodifiableMap(headers);
    }
}
