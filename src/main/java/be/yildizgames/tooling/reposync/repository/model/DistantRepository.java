package be.yildizgames.tooling.reposync.repository.model;

public class DistantRepository implements Repository {

    private final Repository repository;

    private final String id;

    public DistantRepository(Repository repository, String id) {
        this.repository = repository;
        this.id = id;
    }

    @Override
    public String getName() {
        return this.repository.getName();
    }

    @Override
    public LocalRepository cloneOrUpdate(RepositoryHost from, String rootProjectsPath) {
        return this.repository.cloneOrUpdate(from, rootProjectsPath);
    }

    @Override
    public String toString() {
        return this.getName();
    }

    public String getId() {
        return id;
    }
}
