/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */
package be.yildizgames.tooling.reposync.repository.model.github;

import be.yildizgames.tooling.reposync.repository.infrastructure.http.HttpResponse;
import be.yildizgames.tooling.reposync.repository.model.BaseHost;
import be.yildizgames.tooling.reposync.repository.model.BaseRepository;
import be.yildizgames.tooling.reposync.repository.model.DistantRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Grégory Van den Borre
 */
public class Github extends BaseHost {

    private static final String CI_URL = "https://api.travis-ci.org";

    private static final String API_URL = "https://api.github.com";

    @Override
    public String getDomain() {
        return "github.com";
    }

    @Override
    public String getUser() {
        return "yildiz-online";
    }

    @Override
    public String getUrl(String repoName) {
        return "https://" + this.getDomain() + "/" + this.getUser() + "/" + repoName + ".git";
    }

    @Override
    public String getTeam() {
        return "";
    }

    @Override
    public List<DistantRepository> getRepositoryList() {
        List<DistantRepository> result = new ArrayList<>();
        String nextUrl = API_URL + "/users/" + this.getUser() + "/repos";
        while(nextUrl != null) {
            HttpResponse r = caller.get(nextUrl);
            RepoData[] response = parser.parse(r.getContent(), RepoData[].class);
            result.addAll(Arrays.stream(response)
                    .map(rd -> new DistantRepository(BaseRepository.fromName(rd.getName()), rd.getId()))
                    .collect(Collectors.toList()));
            nextUrl = this.computeNextUrl(r.getHeaders().get("Link").get(0));
        }
        return result;
    }

    private String computeNextUrl(String raw) {
        String[] entries =  raw.split(",");
        for(String entry : entries) {
            if(entry.contains("rel=\"next\"")) {
                return entry.replace("<", "").split(">;")[0];
            }
        }
        return null;
    }

    @Override
    public void setDescription(String description, DistantRepository r) {
        //FIXME 404
        this.caller.patch(API_URL + "/repos/" + this.getUser() + "/" + r.getId(), "{\"name\":\"" + r.getName() + "\", \"description\":" + "\"" + description + "\"}");
    }

    @Override
    public void addEnvVariable(String key, String value, DistantRepository r) {
        //TODO implements.
    }

    @Override
    public String getName() {
        return "github";
    }
}
