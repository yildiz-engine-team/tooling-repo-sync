package be.yildizgames.tooling.reposync.repository.model;

public interface Repository {

    String getName();

    LocalRepository cloneOrUpdate(RepositoryHost from, String rootProjectsPath);
}
