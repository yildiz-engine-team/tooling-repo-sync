/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */
package be.yildizgames.tooling.reposync.repository.model.bitbucket;

import be.yildizgames.tooling.reposync.repository.model.BaseHost;
import be.yildizgames.tooling.reposync.repository.model.BaseRepository;
import be.yildizgames.tooling.reposync.repository.model.DistantRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Grégory Van den Borre
 */
public class Bitbucket extends BaseHost {


    private static final String API_URL = "https://api.bitbucket.org/2.0/";

    @Override
    public String getDomain() {
        return "bitbucket.org";
    }

    @Override
    public String getUser() {
        return "yildiz-engine-team";
    }

    @Override
    public String getUrl(String repoName) {
        return "https://" + this.getUser() + "@" + this.getDomain() + "/" + this.getTeam() + "/" + repoName + ".git";
    }

    @Override
    public String getTeam() {
        return "yildiz-engine-team";
    }

    @Override
    public List<DistantRepository> getRepositoryList() {
        List<DistantRepository> result = new ArrayList<>();
        String nextUrl = API_URL + "repositories/" + this.getUser();
        while(nextUrl != null) {
            Response response = parser.parse(caller.get(nextUrl).getContent(), Response.class);
            result.addAll(response.getValues()
                    .stream()
                    .map(rd -> new DistantRepository(BaseRepository.fromName(rd.getName()), rd.getUuid()))
                    .collect(Collectors.toList()));
            nextUrl = response.getNext();
        }
        return result;

    }

    @Override
    public void setDescription(String description, DistantRepository r) {
        this.caller
                .authenticateWith(this.getUser(), this.getPassword())
                .put(
                API_URL + "repositories/" + this.getUser() + "/" + r.getName(),
                "{\"description\": \"" + description + "\"}");
    }

    @Override
    public void addEnvVariable(String key, String value, DistantRepository r) {
        //TODO implements
    }

    @Override
    public String getName() {
        return "origin";
    }
}
