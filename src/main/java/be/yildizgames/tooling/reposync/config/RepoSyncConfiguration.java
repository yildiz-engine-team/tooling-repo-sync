/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */
package be.yildizgames.tooling.reposync.config;


import be.yildizgames.common.exception.implementation.ImplementationException;
import be.yildizgames.common.exception.initialization.InitializationException;
import be.yildizgames.tooling.reposync.repository.model.BaseRepository;
import be.yildizgames.tooling.reposync.repository.model.HostCredentials;
import be.yildizgames.tooling.reposync.repository.model.Repository;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 * Configuration file for the application, contains the credentials for the different SCM servers and the local root project path.
 * @author Grégory Van den Borre
 */
public class RepoSyncConfiguration {

    /**
     * Credential for the Bitbucket scm server.
     */
    private HostCredentials bitbucket;

    /**
     * Credential for the Github scm server.
     */
    private HostCredentials github;

    /**
     * Path to store the projects.
     */
    private String rootProjectsPath;

    private List<Repository> repositoryToIgnore = new ArrayList<>();

    /**
     * Create a configuration from a properties file.
     * The expected keys in the file are:
     * bitbucket.login
     * bitbucket.password
     * github.login
     * github.password
     * projects.root
     * @param path Configuration file path.
     * @return The created configuration.
     * @throws InitializationException If the file cannot be read.
     */
    public static RepoSyncConfiguration fromFile(Path path) {
        ImplementationException.throwForNull(path);
        RepoSyncConfiguration config = new RepoSyncConfiguration();
        Properties p = readConfig(path);
        //FIXME throw initialization exception if properties does not contains the correct key values.
        config.bitbucket = new HostCredentials(p.getProperty("bitbucket.login"), p.getProperty("bitbucket.password"), p.getProperty("pipeline.token"));
        config.github = new HostCredentials(p.getProperty("github.login"), p.getProperty("github.password"), p.getProperty("travis.token"));
        config.rootProjectsPath = p.getProperty("projects.root");
        config.repositoryToIgnore = Arrays
                .stream(p.getProperty("projects.ignore").split(","))
                .map(BaseRepository::fromName)
                .collect(Collectors.toList());
        return config;
    }

    private static Properties readConfig(Path path) {
        Properties prop = new Properties();
        try (InputStream input = Files.newInputStream(path)) {
            prop.load(input);
        } catch (IOException e) {
            InitializationException.invalidConfigurationFile(path, e);
        }
        return prop;
    }

    public HostCredentials getBitbucket() {
        return this.bitbucket;
    }

    public HostCredentials getGithub() {
        return this.github;
    }

    public String getRootProjectsPath() {
        return this.rootProjectsPath;
    }

    public boolean isIgnored(Repository r) {
        for(Repository i : this.repositoryToIgnore) {
            if(r.getName().equals(i.getName())) {
                return true;
            }
        }
        return false;
    }
}
